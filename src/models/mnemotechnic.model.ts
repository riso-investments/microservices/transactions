export interface Mnemotechnic {
  readonly id: number;
  readonly name: string;
  readonly code: string;
  readonly opening_price: number;
  readonly closing_price: number;
  readonly last_update: Date;
}
