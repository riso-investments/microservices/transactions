import { MnemotechnicEntity } from "src/entities/mnemotechnic.entity";
import { UserEntity } from "src/entities/user.entity";

export interface Transaction {
  readonly id: number;
  readonly user: UserEntity;
  readonly mnemotechnic: MnemotechnicEntity;
  readonly date: Date;
  readonly type: number;
  readonly shares_amount: number;
  readonly share_price: number;
  readonly notes: string;
}
