export interface User {
  readonly id: number;
  readonly identification: string;
  readonly name: string;
  readonly birthdate: Date;
}
