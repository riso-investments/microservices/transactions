import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { TransactionEntity } from './transaction.entity';
import { Mnemotechnic } from 'src/models/mnemotechnic.model';

@Entity('mnemotechnics')
export class MnemotechnicEntity extends BaseEntity implements Mnemotechnic {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public name: string;

  @Column()
  public code: string;

  @Column({type: 'timestamp'})
  public last_update: Date;

  @Column({type: 'double'})
  public opening_price: number;

  @Column({type: 'double'})
  public closing_price: number;

  @OneToMany(() => TransactionEntity, transaction => transaction.user)
  public transactions: TransactionEntity[];
}
