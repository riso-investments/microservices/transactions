import { MnemotechnicEntity } from "./mnemotechnic.entity";
import { TransactionEntity } from "./transaction.entity";
import { UserEntity } from "./user.entity";

export const repositories = [TransactionEntity, UserEntity, MnemotechnicEntity]
