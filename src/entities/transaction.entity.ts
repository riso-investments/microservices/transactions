import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { BaseEntity } from './base.entity';
import { Transaction } from 'src/models/transaction.model'
import { UserEntity } from './user.entity';
import { MnemotechnicEntity } from './mnemotechnic.entity';

@Entity('transactions')
export class TransactionEntity extends BaseEntity implements Transaction {
  @PrimaryGeneratedColumn()
  public id: number;

  @ManyToOne(() => UserEntity, user => user.transactions)
  public user: UserEntity;

  @ManyToOne(() => MnemotechnicEntity, mnemotechnic => mnemotechnic.transactions)
  public mnemotechnic: MnemotechnicEntity;

  @Column({type: 'timestamp'})
  public date: Date;

  @Column()
  public type: number;

  @Column()
  public shares_amount: number;

  @Column()
  public share_price: number;

  @Column({default: ''})
  public notes: string;
}
