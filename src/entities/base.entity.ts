export class BaseEntity {
  public assign(dto: any): any {
    Object.assign(this, dto);
    return this;
  }
}
