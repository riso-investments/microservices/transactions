import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { User } from 'src/models/user.model';
import { TransactionEntity } from './transaction.entity';

@Entity('users')
export class UserEntity extends BaseEntity implements User {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public identification: string;

  @Column()
  public name: string;

  @Column({type: 'timestamp'})
  public birthdate: Date;

  @OneToMany(() => TransactionEntity, transaction => transaction.user)
  public transactions: TransactionEntity[];
}
