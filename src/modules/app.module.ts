import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { repositories } from 'src/entities';
import { TransactionModule } from './transaction/transaction.modules'

@Module({
  imports: [
    ConfigModule.forRoot(),
    TransactionModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'admin',
      password: 'password',
      database: 'riso-investments',
      entities: repositories,
      synchronize: true,
    })
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
