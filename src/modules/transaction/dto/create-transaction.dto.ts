import { IsNotEmpty, IsNumber } from 'class-validator';

export class CreateTransactionDto {
  public user: number;
  @IsNotEmpty()
  @IsNumber()
  readonly mnemotechnic: number;

  @IsNotEmpty()
  @IsNumber()
  readonly type: number;

  @IsNotEmpty()
  @IsNumber()
  readonly shares_amount: number;
  
  @IsNotEmpty()
  @IsNumber()
  readonly share_price: number;
}
