import { IsOptional, IsNumber, IsDate } from 'class-validator';

export class UpdateTransactionDto {
  @IsOptional()
  @IsNumber()
  readonly mnemotechnic: number;

  @IsOptional()
  @IsDate()
  readonly date: Date;

  @IsOptional()
  @IsNumber()
  readonly type: number;

  @IsOptional()
  @IsNumber()
  readonly shares_amount: number;

  @IsOptional()
  @IsNumber()
  readonly share_price: number;

  @IsOptional()
  @IsNumber()
  readonly notes: string;
}
