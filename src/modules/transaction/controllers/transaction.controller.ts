import { Body, Controller, Get, HttpCode, Param, Patch, Post } from '@nestjs/common';
import { Transaction } from 'src/models/transaction.model';
import { TransactionService } from 'src/services/transaction/transaction/transaction.service';
import { CreateTransactionDto } from '../dto/create-transaction.dto';
import { UpdateTransactionDto } from '../dto/update-transaction.dto';

@Controller('users/:user/transactions')
export class TransactionController {
  constructor(private readonly transactionSevice: TransactionService) {}

  @Get()
  public async getTransactionsByUser(@Param() params): Promise<Transaction[]> {
    return await this.transactionSevice.getAllByUser(params.user);
  }

  @Get(':id')
  public async getTransactions(@Param('id') id: number, @Param('user') user: number): Promise<Transaction> {
    return await this.transactionSevice.findOne(id, user);
  }

  @Post()
  @HttpCode(201)
  public async createTransactionByUser(@Param() params, @Body() dto: CreateTransactionDto): Promise<Transaction> {
    dto.user = Number(params.user);
    return await this.transactionSevice.create(dto);
  }

  @Patch(':id')
  public async updateHello(@Param('id') id: number, @Body() dto: UpdateTransactionDto): Promise<Transaction> {
    return this.transactionSevice.update(id, dto);
  }
}
