import { Module } from '@nestjs/common';
import { TransactionServiceModule } from 'src/services/transaction/transaction-service.module';
import { TransactionController } from './controllers/transaction.controller'

@Module({
  imports: [TransactionServiceModule],
  controllers: [TransactionController]
})
export class TransactionModule {}
