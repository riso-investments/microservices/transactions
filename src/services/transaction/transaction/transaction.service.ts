import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TransactionEntity } from 'src/entities/transaction.entity';
import { Transaction } from 'src/models/transaction.model';
import { CreateTransactionDto } from 'src/modules/transaction/dto/create-transaction.dto';
import { UpdateTransactionDto } from 'src/modules/transaction/dto/update-transaction.dto';
import { Repository } from 'typeorm';

@Injectable()
export class TransactionService {
  constructor(
    @InjectRepository(TransactionEntity) private transactionRepository: Repository<TransactionEntity>
) {}

  public async getAllByUser(user: number): Promise<Transaction[]> {
    return this.transactionRepository.find({
      relations: ['user', 'mnemotechnic'],
      where: { user }
    });
  }

  public async findOne(id: number, user: number): Promise<Transaction> {
    return this.transactionRepository.findOne({
      relations: ['user', 'mnemotechnic'],
      where: { id, user }
    });
  }

  public async create(dto: CreateTransactionDto): Promise<Transaction> {
    const transaction = this.transactionRepository.create().assign(dto);

    await this.transactionRepository.save(transaction);
    
    return transaction;
  }

  public async update(id: number, dto: UpdateTransactionDto): Promise<Transaction> {
    await this.transactionRepository.update(id, dto as any);

    return await this.transactionRepository.findOne({
      relations: ['user', 'mnemotechnic'],
      where: { id }
    });
  }
}
