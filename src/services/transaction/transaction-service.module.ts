import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TransactionEntity } from 'src/entities/transaction.entity';
import { TransactionService } from './transaction/transaction.service';

@Module({
  imports: [ConfigModule, TypeOrmModule.forFeature([TransactionEntity])],
  providers: [TransactionService],
  exports: [TransactionService]
})
export class TransactionServiceModule {}
