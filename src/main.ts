import { NestFactory } from '@nestjs/core';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AppModule } from './modules/app.module';
import { join } from 'path';

async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter()
  );
  app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.GRPC,
    options: {
      package: 'br',
      protoPath: join(__dirname, 'protos/transactions.proto'),
      url: process.env.GRPC_HOST || '0.0.0.0:50051'
    }
  });

  await app.startAllMicroservicesAsync();
  await app.listen(Number(process.env.APP_PORT || '3000'), process.env.APP_HOST || '0.0.0.0');
}
bootstrap();
