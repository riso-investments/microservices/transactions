# Transactions
## Description



## Installation

```bash
$ nvm use
$ yarn install
```

## Running the app

```bash
$ yarn start:dev
```

## Test

```bash
# unit tests
$ yarn test

# e2e tests
$ yarn test:e2e

# test coverage
$ yarn test:cov
```
